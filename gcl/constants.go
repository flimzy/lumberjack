package gcl

// Common log fields.
const (
	FieldError          = "error"
	FieldRequest        = "request"
	FieldLatency        = "latency"
	FieldResponseSize   = "response_size"
	FieldRequestSize    = "request_size"
	FieldResponseStatus = "response_status"
	FieldRemoteIP       = "remote_ip"
)
