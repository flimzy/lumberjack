package gcl

import "github.com/pkg/errors"

// extractStacktrace extracts the deepest (inner-most) stack trace, if any.
func extractStacktrace(err error) errors.StackTrace {
	var stackErr interface {
		error
		StackTrace() errors.StackTrace
	}
	var stack errors.StackTrace
	for errors.As(err, &stackErr) {
		stack = stackErr.StackTrace()
		err = unwrapJoined(err)
		if err == nil {
			return stack
		}
	}
	return stack
}

// unwrapJoined works like [errors.Unwrap], but also considers errors wrapped
// by [Join], although it only ever considers the first error in a joined error.
//
//nolint:errorlint // We really do want direct type assertions on errors here.
func unwrapJoined(err error) error {
	if u, ok := err.(interface {
		Unwrap() error
	}); ok {
		return u.Unwrap()
	}
	if u, ok := err.(interface {
		Unwrap() []error
	}); ok {
		for _, err := range u.Unwrap() {
			if err != nil {
				return err
			}
		}
	}
	return nil
}
