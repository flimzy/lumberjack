// Package gcl provides a [log/slog.Handler] for Google Cloud Logging.
package gcl

import (
	"context"
	"log/slog"
	"net/http"
	"runtime"
	"time"

	"cloud.google.com/go/logging"
	logpb "cloud.google.com/go/logging/apiv2/loggingpb"
	"google.golang.org/api/option"

	"gitlab.com/flimzy/lumberjack/handlerhelper"
)

type loggingLogger interface {
	Flush() error
	Log(logging.Entry)
}

// Handler is a [log/slog.Handler] for GCP logging.
type Handler struct {
	client *logging.Client
	logger loggingLogger
	entry  *logging.Entry
	level  slog.Leveler
	acc    handlerhelper.MapAccumulator
}

var _ slog.Handler = (*Handler)(nil)

// New returns a new GCP logging handler.
func New(ctx context.Context, project, logID string, level slog.Leveler, clientOpts ...option.ClientOption) (*Handler, error) {
	client, err := logging.NewClient(ctx, project, clientOpts...)
	if err != nil {
		return nil, err
	}
	logger := client.Logger(logID)
	return newHandler(client, logger, level), nil
}

func newHandler(client *logging.Client, logger loggingLogger, level slog.Leveler) *Handler {
	return &Handler{
		client: client,
		logger: logger,
		level:  level,
	}
}

// Ping creates a test log to ping the connection.
func (h *Handler) Ping(ctx context.Context) error {
	return h.client.Ping(ctx)
}

// Flush blocks until any outstanding logs have been flushed.
func (h *Handler) Flush() error {
	return h.logger.Flush()
}

// Close flushes any outstanding logs, and closes the client connection.
func (h *Handler) Close() error {
	return h.client.Close()
}

// Enabled returns true if level is at least to the configured minimum level.
func (h *Handler) Enabled(_ context.Context, level slog.Level) bool {
	minLevel := slog.LevelInfo
	if h.level != nil {
		minLevel = h.level.Level()
	}
	return level >= minLevel
}

func (h *Handler) clone() *Handler {
	var entryCopy *logging.Entry
	if h.entry != nil {
		entryCopy = new(logging.Entry)
		*entryCopy = *h.entry
	}
	return &Handler{
		entry:  entryCopy,
		client: h.client,
		logger: h.logger,
		level:  h.level,
		acc:    h.acc,
	}
}

// Handle logs r to Google Cloud Logging.
func (h *Handler) Handle(_ context.Context, r slog.Record) error {
	payload := h.acc.ResultF(r, h.filterAttr)
	payload["msg"] = r.Message
	entry := h.entry
	if entry == nil {
		entry = &logging.Entry{}
	}
	entry.Timestamp = r.Time
	entry.Payload = payload
	entry.Severity = severityMap[r.Level]

	h.logger.Log(*entry)
	return nil
}

//nolint:gochecknoglobals // Maps can't be constants
var severityMap = map[slog.Level]logging.Severity{
	slog.LevelDebug: logging.Debug,
	slog.LevelInfo:  logging.Info,
	slog.LevelWarn:  logging.Warning,
	slog.LevelError: logging.Error,
}

func (h *Handler) setRequest(req *http.Request) {
	if h.entry == nil {
		h.entry = &logging.Entry{}
	}
	if h.entry.HTTPRequest == nil {
		h.entry.HTTPRequest = &logging.HTTPRequest{}
	}
	h.entry.HTTPRequest.Request = req
}

func (h *Handler) setLatency(latency time.Duration) {
	if h.entry == nil {
		h.entry = &logging.Entry{}
	}
	if h.entry.HTTPRequest == nil {
		h.entry.HTTPRequest = &logging.HTTPRequest{}
	}
	h.entry.HTTPRequest.Latency = latency
}

func (h *Handler) setResponseSize(size int64) {
	if h.entry == nil {
		h.entry = &logging.Entry{}
	}
	if h.entry.HTTPRequest == nil {
		h.entry.HTTPRequest = &logging.HTTPRequest{}
	}
	h.entry.HTTPRequest.ResponseSize = size
}

func (h *Handler) setRequestSize(size int64) {
	if h.entry == nil {
		h.entry = &logging.Entry{}
	}
	if h.entry.HTTPRequest == nil {
		h.entry.HTTPRequest = &logging.HTTPRequest{}
	}
	h.entry.HTTPRequest.RequestSize = size
}

func (h *Handler) setResponseStatus(status int64) {
	if h.entry == nil {
		h.entry = &logging.Entry{}
	}
	if h.entry.HTTPRequest == nil {
		h.entry.HTTPRequest = &logging.HTTPRequest{}
	}
	h.entry.HTTPRequest.Status = int(status)
}

func (h *Handler) setRemoteIP(ipAddr string) {
	if h.entry == nil {
		h.entry = &logging.Entry{}
	}
	if h.entry.HTTPRequest == nil {
		h.entry.HTTPRequest = &logging.HTTPRequest{}
	}
	h.entry.HTTPRequest.RemoteIP = ipAddr
}

func (h *Handler) setErrorLocation(err error) {
	stack := extractStacktrace(err)
	if len(stack) == 0 {
		return
	}
	if h.entry == nil {
		h.entry = &logging.Entry{}
	}

	pc := uintptr(stack[0])
	fn := runtime.FuncForPC(pc)
	file, line := fn.FileLine(pc)
	h.entry.SourceLocation = &logpb.LogEntrySourceLocation{
		File:     file,
		Line:     int64(line),
		Function: fn.Name(),
	}
}

func (h *Handler) filterAttr(groups []string, attr slog.Attr) slog.Attr {
	if len(groups) > 0 {
		return attr
	}
	switch attr.Key {
	case FieldRequest:
		if req, ok := attr.Value.Any().(*http.Request); ok {
			h.setRequest(req)
			return slog.Attr{}
		}
	case FieldLatency:
		h.setLatency(attr.Value.Duration())
		return slog.Attr{}
	case FieldResponseSize:
		h.setResponseSize(attr.Value.Int64())
		return slog.Attr{}
	case FieldRequestSize:
		h.setRequestSize(attr.Value.Int64())
		return slog.Attr{}
	case FieldResponseStatus:
		h.setResponseStatus(attr.Value.Int64())
		return slog.Attr{}
	case FieldRemoteIP:
		h.setRemoteIP(attr.Value.String())
		return slog.Attr{}
	case FieldError:
		if err, ok := attr.Value.Any().(error); ok {
			h.setErrorLocation(err)
			return slog.String(attr.Key, err.Error()) // To avoid any JSON marshaling shenanigans
		}
	}
	return attr
}

// WithAttrs returns a clone of h, with attrs appended.
func (h *Handler) WithAttrs(attrs []slog.Attr) slog.Handler {
	h2 := h.clone()
	h2.acc = h.acc.WithAttrsF(attrs, h2.filterAttr)
	return h2
}

// WithGroup returns a clone of h, with the named group added.
func (h *Handler) WithGroup(name string) slog.Handler {
	h2 := h.clone()
	h2.acc = h.acc.WithGroup(name)
	return h2
}
