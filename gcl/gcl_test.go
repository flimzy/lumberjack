package gcl

import (
	"log/slog"
	"net/http"
	"testing"

	"cloud.google.com/go/logging"
	logpb "cloud.google.com/go/logging/apiv2/loggingpb"
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"github.com/pkg/errors"
)

const (
	keyMsg = "msg"
)

type loggerSpy struct {
	logged logging.Entry
}

var _ loggingLogger = (*loggerSpy)(nil)

func (*loggerSpy) Flush() error          { return nil }
func (l *loggerSpy) Log(e logging.Entry) { l.logged = e }

func TestHandler(t *testing.T) {
	tests := []struct {
		name string
		fn   func(*slog.Logger)
		want logging.Entry
	}{
		{
			name: "simple log",
			fn: func(l *slog.Logger) {
				l.Info("info")
			},
			want: logging.Entry{
				Severity: logging.Info,
				Payload:  map[string]any{keyMsg: "info"},
			},
		},
		{
			name: "attrs",
			fn: func(l *slog.Logger) {
				l.With("foo", "bar").Info("info2")
			},
			want: logging.Entry{
				Severity: logging.Info,
				Payload: map[string]any{
					"foo":  "bar",
					keyMsg: "info2",
				},
			},
		},
		{
			name: "http request",
			fn: func(l *slog.Logger) {
				l.With(FieldRequest, &http.Request{Method: http.MethodGet}).Info("req")
			},
			want: logging.Entry{
				Severity: logging.Info,
				Payload: map[string]any{
					keyMsg: "req",
				},
				HTTPRequest: &logging.HTTPRequest{
					Request: &http.Request{Method: http.MethodGet},
				},
			},
		},
		{
			name: "error with stack trace",
			fn: func(l *slog.Logger) {
				l.With(FieldError, errors.New("oh noes")).Info("reqx")
			},
			want: logging.Entry{
				Severity: logging.Info,
				Payload: map[string]any{
					"error": "oh noes",
					keyMsg:  "reqx",
				},
				SourceLocation: &logpb.LogEntrySourceLocation{
					Function: "gitlab.com/flimzy/lumberjack/gcl.TestHandler.func4",
				},
			},
		},
		{
			name: "clone entry by adding attr after clone",
			fn: func(l *slog.Logger) {
				l.With(FieldError, errors.New("oh noes")).With("asdf", "qwerty").Info("reqz")
			},
			want: logging.Entry{
				Severity: logging.Info,
				Payload: map[string]any{
					"error": "oh noes",
					"asdf":  "qwerty",
					keyMsg:  "reqz",
				},
				SourceLocation: &logpb.LogEntrySourceLocation{
					Function: "gitlab.com/flimzy/lumberjack/gcl.TestHandler.func5",
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &loggerSpy{}
			h := newHandler(nil, l, slog.LevelDebug)
			sl := slog.New(h)
			tt.fn(sl)
			if d := cmp.Diff(tt.want, l.logged,
				cmpopts.IgnoreFields(logpb.LogEntrySourceLocation{}, "Line", "File"),
				cmpopts.IgnoreFields(logging.Entry{}, "Timestamp"),
				cmpopts.IgnoreUnexported(logpb.LogEntrySourceLocation{}, http.Request{}),
			); d != "" {
				t.Error(d)
			}
		})
	}
}
