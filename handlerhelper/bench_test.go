package handlerhelper

import (
	"context"
	"log/slog"
	"testing"
)

type testHandler1 struct {
	*Helper
	core *core
}

var _ slog.Handler = (*testHandler1)(nil)

type core struct {
	results []any
}

// Enabled returns true if level is at least to the configured minimum level.
func (*core) Enabled(context.Context, slog.Level) bool {
	return true
}

// Handle logs r to Google Cloud Logging.
func (h *core) Handle(_ context.Context, r slog.Record) error {
	h.results = append(h.results, AttrsToMap(r, nil))
	return nil
}

func BenchmarkTestHandler1(b *testing.B) {
	core := &core{}
	h := testHandler1{
		Helper: NewHelper(core),
		core:   core,
	}
	s := slog.New(h)

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		s.Info("info", "n", n)
	}
}

type testHandler2 struct {
	acc     MapAccumulator
	results []any
}

var _ slog.Handler = (*testHandler2)(nil)

// Enabled returns true if level is at least to the configured minimum level.
func (testHandler2) Enabled(context.Context, slog.Level) bool {
	return true
}

// Handle logs r to Google Cloud Logging.
func (h *testHandler2) Handle(_ context.Context, r slog.Record) error {
	h.results = append(h.results, AttrsToMap(r, nil))
	return nil
}

func (h *testHandler2) WithAttrs(attrs []slog.Attr) slog.Handler {
	return &testHandler2{
		acc:     h.acc.WithAttrs(attrs),
		results: h.results,
	}
}

func (h *testHandler2) WithGroup(name string) slog.Handler {
	return &testHandler2{
		acc:     h.acc.WithGroup(name),
		results: h.results,
	}
}

func BenchmarkTestHandler2(b *testing.B) {
	h := &testHandler2{}
	s := slog.New(h)

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		s.Info("info", "n", n)
	}
}
