// Package handlerhelper provides an [log/slog.Handler] helper, to simplify the
// implementation of a [log/slog.Handler].
package handlerhelper

import (
	"context"
	"log/slog"
)

// Handler is a simplified version of [log/slog.Handler]. All calls to
// [log/slog.Handler.WithAttrs] and [log/slog.Handler.WithGroup] are accumulated
// and passed to Handle as Attrs on the Record argument.
type Handler interface {
	Enabled(context.Context, slog.Level) bool
	Handle(context.Context, slog.Record) error
}

// minAttrs defines the capacity for a new attrs slice. This number is copied
// from comments in the sdlib, that 5 seems to be an optimal number.
const minAttrs = 5

// NewHelper converts a Handler into an slog.Handler.
func NewHelper(h Handler) *Helper {
	return &Helper{
		handler: h,
		attrs:   make([]slog.Attr, 0, minAttrs),
	}
}

// Helper is a [log/slog.Handler].
type Helper struct {
	handler Handler
	attrs   []slog.Attr
	groups  []string
}

func (h *Helper) clone() *Helper {
	attrs := make([]slog.Attr, len(h.attrs)+minAttrs)
	copy(attrs, h.attrs)
	groups := make([]string, len(h.groups))
	copy(groups, h.groups)
	return &Helper{
		handler: h.handler,
		attrs:   attrs,
		groups:  groups,
	}
}

var _ slog.Handler = (*Helper)(nil)

// Enabled reports whether the underlying handler handles records at the given level.
func (h *Helper) Enabled(ctx context.Context, level slog.Level) bool {
	return h.handler.Enabled(ctx, level)
}

// WithAttrs accumulates attrs, to be passed to the underlying handler's Handle
// call.
func (h *Helper) WithAttrs(attrs []slog.Attr) slog.Handler {
	h2 := h.clone()

	h2.attrs = appendAttrsToGroup(h2.groups, h2.attrs, attrs)
	return h2
}

// WithGroup accumulates the group to be passed to the underlying handler's
// Handle call.
func (h *Helper) WithGroup(name string) slog.Handler {
	h2 := h.clone()
	h2.groups = append(h2.groups, name)
	return h2
}

// Handle materializes any accumulated attrs and groups passed to previous calls
// of WithAttrs or WithGroup, prepends them to r's Attrs, then calls the
// underlying handler's Handle method.
func (h *Helper) Handle(ctx context.Context, r slog.Record) error {
	r2 := slog.NewRecord(r.Time, r.Level, r.Message, r.PC)

	rattrs := make([]slog.Attr, 0, r.NumAttrs())
	r.Attrs(func(a slog.Attr) bool {
		rattrs = append(rattrs, a)
		return true
	})
	h2 := h.clone()
	r2.AddAttrs(appendAttrsToGroup(h2.groups, h2.attrs, rattrs)...)

	return h2.handler.Handle(ctx, r2)
}
