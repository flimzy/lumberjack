package handlerhelper

import (
	"bytes"
	"io"
	"log/slog"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
)

func TestHelper(t *testing.T) {
	opts := &slog.HandlerOptions{
		ReplaceAttr: func(_ []string, a slog.Attr) slog.Attr {
			if _, ok := a.Value.Any().(time.Time); !ok {
				return a
			}
			return slog.Attr{}
		},
	}

	createLogs := func(l *slog.Logger) {
		l.Debug("debug")
		l.Info("info")
		l.Error("error")
		l.Info("info", "foo", "bar")
		l.With("foo", "bar").Info("info")
		l.WithGroup("grp").Info("info", "moo", "cow")
		l.With(slog.Group("grp", "a", "a")).Info("info", "b", "b")
		l.With("a", "a").WithGroup("X").With(slog.Group("y", "y", "y")).With("z", "z").Info("info", "aa", "aa")
	}

	controlBuf := &bytes.Buffer{}
	controlLog := slog.New(slog.NewTextHandler(controlBuf, opts))

	createLogs(controlLog)

	treatmentBuf := &bytes.Buffer{}
	treatmentLog := slog.New(NewHelper(slog.NewTextHandler(treatmentBuf, opts)))

	createLogs(treatmentLog)

	if d := cmp.Diff(controlBuf.String(), treatmentBuf.String()); d != "" {
		t.Error(d)
	}
}

func BenchmarkStandard(b *testing.B) {
	log := slog.New(slog.NewTextHandler(io.Discard, nil))

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		log.With("a", "b").Info("log", "n", n)
	}
}

func BenchmarkHelped(b *testing.B) {
	log := slog.New(NewHelper(slog.NewTextHandler(io.Discard, nil)))

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		log.With("a", "b").Info("log", "n", n)
	}
}
