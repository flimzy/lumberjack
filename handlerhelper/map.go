package handlerhelper

import (
	"log/slog"
)

// FilterFunc works exactly like [log/slog.HandlerOptions.ReplaceAttr], and can
// be used to modify or eliminate attributes in the output.
type FilterFunc func(groups []string, a slog.Attr) slog.Attr

func nilFilter(_ []string, a slog.Attr) slog.Attr { return a }

// AttrsToMap iterats over r.Attrs, calling filter if it's defined, and
// converting the results to a nested map of keys and values.
func AttrsToMap(r slog.Record, filter FilterFunc) map[string]any {
	if filter == nil {
		filter = nilFilter
	}
	result := make(map[string]any, r.NumAttrs())
	if r.NumAttrs() == 0 {
		return result
	}
	const groupCap = 2
	groups := make([]string, 0, groupCap)
	r.Attrs(func(att slog.Attr) bool {
		accumulateMap(result, groups, filter, att)
		return true
	})

	return result
}

func isEmpty(a slog.Attr) bool {
	if a.Key != "" {
		return false
	}
	return a.Value.Equal(slog.Value{})
}

func accumulateMap(target map[string]any, groups []string, filter FilterFunc, atts ...slog.Attr) {
	for _, att := range atts {
		att = filter(groups, att)
		if isEmpty(att) {
			continue
		}
		switch att.Value.Kind() {
		case slog.KindGroup:
			attrs := att.Value.Group()
			targetGroup, ok := target[att.Key].(map[string]any)
			if !ok {
				targetGroup = make(map[string]any, len(attrs))
				target[att.Key] = targetGroup
			}
			accumulateMap(targetGroup, append(groups, att.Key), filter, attrs...)
		case slog.KindString:
			target[att.Key] = att.Value.String()
		default:
			target[att.Key] = att.Value.Any()
		}
	}
}

// MapAccumulator accumulates [log/slog.Attr] values into a map[string]any. The
// zero value is usable.
type MapAccumulator struct {
	groups []string
	attrs  []map[string]any
}

func (a MapAccumulator) clone() MapAccumulator {
	return MapAccumulator{
		groups: a.groups,
		attrs:  append(a.attrs, make(map[string]any, minAttrs)),
	}
}

func addAttrs(target map[string]any, groups []string,
	filter func(groups []string, a slog.Attr) slog.Attr,
	attrs ...slog.Attr,
) {
	for _, attr := range attrs {
		if isEmpty(attr) {
			continue
		}
		switch attr.Value.Kind() {
		case slog.KindGroup:
			newTarget, found := target[attr.Key].(map[string]any)
			if !found {
				newTarget = make(map[string]any, minAttrs)
			}
			addAttrs(newTarget, append(groups, attr.Key), filter, attr.Value.Group()...)
			if len(newTarget) > 0 {
				target[attr.Key] = newTarget
			}
		default:
			if filter != nil {
				addAttrs(target, groups, nil, filter(groups, attr))
				continue
			}
			target[attr.Key] = attr.Value.Any()
		}
	}
}

// WithAttrsF returns a clone of a with attrs filtered and accumulated.
func (a MapAccumulator) WithAttrsF(attrs []slog.Attr, filter FilterFunc) MapAccumulator {
	a2 := a.clone()
	target := a2.attrs[len(a2.attrs)-1]
	for _, g := range a2.groups {
		newTarget := make(map[string]any, minAttrs)
		target[g] = newTarget
		target = newTarget
	}
	addAttrs(target, nil, filter, attrs...)
	return a2
}

// WithAttrs returns a clone of a with attrs accumulated.
func (a MapAccumulator) WithAttrs(attrs []slog.Attr) MapAccumulator {
	return a.WithAttrsF(attrs, nil)
}

// WithGroup returns a clone of a, with new group name accumulated.
func (a MapAccumulator) WithGroup(name string) MapAccumulator {
	a2 := a.clone()
	a2.groups = append(a2.groups, name)
	return a2
}

// ResultF returns the final filtered result of the accumulated attributes.
func (a MapAccumulator) ResultF(r slog.Record, filter FilterFunc) map[string]any {
	recAttrs := make([]slog.Attr, 0, r.NumAttrs())
	r.Attrs(func(attr slog.Attr) bool {
		recAttrs = append(recAttrs, attr)
		return true
	})
	a2 := a.WithAttrsF(recAttrs, filter)
	result := make(map[string]any, len(a2.attrs))
	for i := len(a2.attrs); i > 0; i-- {
		merge(result, a2.attrs[i-1])
	}
	return result
}

// Result returns the final result of the accumulated attributes.
func (a MapAccumulator) Result(r slog.Record) map[string]any {
	return a.ResultF(r, nil)
}

// merge merges source into target, skipping any existing keys. target will
// be modified, and source is left unmodified. The function will panic if target
// is nil.
func merge(target, source map[string]any) {
	if target == nil {
		panic("target must not be nil")
	}
	for k, v := range source {
		switch t := target[k].(type) {
		case map[string]any:
			if s, ok := source[k].(map[string]any); ok {
				merge(t, s)
				continue
			}
		default:
			if _, found := target[k]; !found {
				target[k] = v
			}
		}
	}
}
