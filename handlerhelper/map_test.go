package handlerhelper

import (
	"log/slog"
	"strconv"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
)

func TestAttrsToMap(t *testing.T) {
	tests := []struct {
		name   string
		attrs  []slog.Attr
		filter FilterFunc
		want   map[string]any
	}{
		{
			name: "nothing",
			want: map[string]any{},
		},
		{
			name:  "single key/value",
			attrs: []slog.Attr{slog.Any("foo", "bar")},
			want:  map[string]any{"foo": "bar"},
		},
		{
			name:  "override previous key",
			attrs: []slog.Attr{slog.Any("fox", "bar"), slog.Any("fox", "bax")},
			want:  map[string]any{"fox": "bax"},
		},
		{
			name:  "empty group",
			attrs: []slog.Attr{slog.Any("a", "b"), slog.Group("g")},
			want:  map[string]any{"a": "b"},
		},
		{
			name:  "group",
			attrs: []slog.Attr{slog.Group("x", "y", "z")},
			want: map[string]any{
				"x": map[string]any{"y": "z"},
			},
		},
		{
			name:  "merge groups",
			attrs: []slog.Attr{slog.Group("xa", "yy", "zz"), slog.Group("xa", "1", "2")},
			want: map[string]any{
				"xa": map[string]any{
					"yy": "zz",
					"1":  "2",
				},
			},
		},
		{
			name:   "filter to empty",
			attrs:  []slog.Attr{slog.Any("asdf", "fdas")},
			filter: func([]string, slog.Attr) slog.Attr { return slog.Attr{} },
			want:   map[string]any{},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := slog.NewRecord(time.Time{}, slog.LevelDebug, "", 0)
			r.AddAttrs(tt.attrs...)
			got := AttrsToMap(r, tt.filter)
			if d := cmp.Diff(tt.want, got); d != "" {
				t.Error(d)
			}
		})
	}
}

func BenchmarkAttrsToMap_0_attrs(b *testing.B) {
	r := slog.NewRecord(time.Time{}, slog.LevelDebug, "", 0)

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		_ = AttrsToMap(r, nil)
	}
}

func BenchmarkAttrsToMap_1_attr(b *testing.B) {
	r := slog.NewRecord(time.Time{}, slog.LevelDebug, "", 0)
	r.AddAttrs(slog.Any("asef", "asdf"))

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		_ = AttrsToMap(r, nil)
	}
}

func BenchmarkAttrsToMap_10_attrs(b *testing.B) {
	r := slog.NewRecord(time.Time{}, slog.LevelDebug, "", 0)
	const attrCount = 10
	for i := 0; i < attrCount; i++ {
		r.AddAttrs(slog.Any(strconv.Itoa(i), "asdf"))
	}

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		_ = AttrsToMap(r, nil)
	}
}

func BenchmarkAttrsToMap_10_attr_and_groups(b *testing.B) {
	r := slog.NewRecord(time.Time{}, slog.LevelDebug, "", 0)
	const attrCount = 10
	for i := 0; i < attrCount; i++ {
		r.AddAttrs(slog.Group(strconv.Itoa(i), slog.Any("fdaa", "asd")))
	}

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		_ = AttrsToMap(r, nil)
	}
}

func Test_merge(t *testing.T) {
	const (
		group  = "g"
		key1   = "a"
		value1 = 1
		key2   = "b"
		value2 = 2
	)
	tests := []struct {
		name                 string
		target, source, want map[string]any
	}{
		{
			name:   "source nil",
			target: map[string]any{key1: value1},
			source: nil,
			want:   map[string]any{key1: value1},
		},
		{
			name:   "simple merge",
			target: map[string]any{key1: value1},
			source: map[string]any{key2: value2},
			want:   map[string]any{key1: value1, key2: value2},
		},
		{
			name:   "duplicate key",
			target: map[string]any{key1: value1},
			source: map[string]any{key1: value2},
			want:   map[string]any{key1: value1},
		},
		{
			name:   "merge group",
			target: map[string]any{group: map[string]any{key1: value1}},
			source: map[string]any{group: map[string]any{key2: value2}},
			want:   map[string]any{group: map[string]any{key1: value1, key2: value2}},
		},
		{
			name:   "non-group already exists",
			target: map[string]any{group: value1},
			source: map[string]any{group: map[string]any{key2: value2}},
			want:   map[string]any{group: value1},
		},
		{
			name:   "merge non-group",
			target: map[string]any{group: map[string]any{key1: value1}},
			source: map[string]any{group: value2},
			want:   map[string]any{group: map[string]any{key1: value1}},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			merge(tt.target, tt.source)
			if d := cmp.Diff(tt.want, tt.target); d != "" {
				t.Error(d)
			}
		})
	}
}

//nolint:revive // Allow dupe strings
func TestMapAccumulator_close(t *testing.T) {
	a := MapAccumulator{
		groups: []string{"a", "b", "c"},
		attrs: []map[string]any{
			{"a": "b"},
			{"b": "c"},
		},
	}
	c := a.clone()
	c.groups = append(c.groups, "x")

	wantCopy := MapAccumulator{
		groups: []string{"a", "b", "c", "x"},
		attrs: []map[string]any{
			{"a": "b"},
			{"b": "c"},
			{},
		},
	}
	wantOrig := MapAccumulator{
		groups: []string{"a", "b", "c"},
		attrs: []map[string]any{
			{"a": "b"},
			{"b": "c"},
		},
	}
	if d := cmp.Diff(wantCopy, c, cmp.AllowUnexported(MapAccumulator{})); d != "" {
		t.Errorf("clone: %s", d)
	}
	if d := cmp.Diff(wantOrig, a, cmp.AllowUnexported(MapAccumulator{})); d != "" {
		t.Errorf("orig: %s", d)
	}
}

//nolint:revive // Allow dupe strings
func TestMapAccumulator_WithAttrs(t *testing.T) {
	tests := []struct {
		name   string
		state  MapAccumulator
		attrs  []slog.Attr
		filter FilterFunc
		want   map[string]any
	}{
		{
			name: "no attrs",
			want: map[string]any{},
		},
		{
			name:  "single attr",
			attrs: []slog.Attr{slog.Any("foo", "bar")},
			want:  map[string]any{"foo": "bar"},
		},
		{
			name:  "duplicate keys",
			attrs: []slog.Attr{slog.Any("foo", 1), slog.Any("foo", 2)},
			want:  map[string]any{"foo": int64(2)},
		},
		{
			name:  "group",
			attrs: []slog.Attr{slog.Group("g", slog.Any("x", 3))},
			want:  map[string]any{"g": map[string]any{"x": int64(3)}},
		},
		{
			name:  "clobber non-group",
			attrs: []slog.Attr{slog.Any("foo", "Bar"), slog.Group("foo", slog.Any("x", "y"))},
			want:  map[string]any{"foo": map[string]any{"x": "y"}},
		},
		{
			name:  "clobber group",
			attrs: []slog.Attr{slog.Group("foo", slog.Any("x", "y")), slog.Any("foo", "Bar")},
			want:  map[string]any{"foo": "Bar"},
		},
		{
			name: "pre-defined groups",
			state: MapAccumulator{
				groups: []string{"a", "b"},
			},
			attrs: []slog.Attr{slog.Any("X", "x"), slog.Group("g", slog.Any("Z", "z"))},
			want: map[string]any{
				"a": map[string]any{
					"b": map[string]any{
						"X": "x",
						"g": map[string]any{
							"Z": "z",
						},
					},
				},
			},
		},
		{
			name: "with filter",
			filter: func([]string, slog.Attr) slog.Attr {
				return slog.Attr{}
			},
			attrs: []slog.Attr{slog.Any("X", "x"), slog.Group("g", slog.Any("Z", "z"))},
			want:  map[string]any{},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := tt.state.WithAttrsF(tt.attrs, tt.filter)
			got := a.ResultF(slog.Record{}, tt.filter)
			if d := cmp.Diff(tt.want, got); d != "" {
				t.Error(d)
			}
		})
	}
}

//nolint:revive // Allow dupe strings
func TestMapAccumulator_Result(t *testing.T) {
	tests := []struct {
		name   string
		state  MapAccumulator
		record slog.Record
		want   map[string]any
	}{
		{
			name:   "no attrs",
			record: slog.NewRecord(time.Time{}, slog.LevelDebug, "x", 0),
			want:   map[string]any{},
		},
		{
			name: "single attr",
			record: func() slog.Record {
				r := slog.NewRecord(time.Time{}, slog.LevelDebug, "x", 0)
				r.Add("foo", "bar")
				return r
			}(),
			want: map[string]any{"foo": "bar"},
		},
		{
			name: "existing group",
			state: MapAccumulator{
				groups: []string{"group"},
			},
			record: func() slog.Record {
				r := slog.NewRecord(time.Time{}, slog.LevelDebug, "x", 0)
				r.Add("foo", "bar")
				return r
			}(),
			want: map[string]any{"group": map[string]any{"foo": "bar"}},
		},
		{
			name: "new group",
			state: MapAccumulator{
				groups: []string{"group"},
			},
			record: func() slog.Record {
				r := slog.NewRecord(time.Time{}, slog.LevelDebug, "x", 0)
				r.Add("foo", "bar")
				r.AddAttrs(slog.Group("xyz", "abc", "def"))
				return r
			}(),
			want: map[string]any{
				"group": map[string]any{
					"foo": "bar",
					"xyz": map[string]any{"abc": "def"},
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.state.Result(tt.record)
			if d := cmp.Diff(tt.want, got); d != "" {
				t.Error(d)
			}
		})
	}
}

func BenchmarkMapAccumulator_0_attrs(b *testing.B) {
	var attrs []slog.Attr

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		a := MapAccumulator{}
		_ = a.WithAttrs(attrs).Result(slog.Record{})
	}
}

func BenchmarkMapAccumulator_1_attr(b *testing.B) {
	attrs := []slog.Attr{slog.Any("asef", "asdfx")}

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		a := MapAccumulator{}
		_ = a.WithAttrs(attrs).Result(slog.Record{})
	}
}

func BenchmarkMapAccumulator_10_attrs(b *testing.B) {
	const attrCount = 10
	attrs := make([]slog.Attr, attrCount)
	for i := range attrs {
		attrs[i] = slog.Any(strconv.Itoa(i), "asdy")
	}

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		a := MapAccumulator{}
		_ = a.WithAttrs(attrs).Result(slog.Record{})
	}
}

func BenchmarkMapAccumulator_10_attr_and_groups(b *testing.B) {
	const attrCount = 10
	attrs := make([]slog.Attr, attrCount)
	for i := range attrs {
		attrs[i] = slog.Group(strconv.Itoa(i), slog.Any("fdaa", "asd"))
	}

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		a := MapAccumulator{}
		_ = a.WithAttrs(attrs).Result(slog.Record{})
	}
}
