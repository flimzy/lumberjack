package handlerhelper

import (
	"log/slog"
)

// attrSliceToAny returns a slice with all elements mapped to `any` type
func attrSliceToAny(attrs []slog.Attr) []any {
	result := make([]any, len(attrs))
	for i, item := range attrs {
		result[i] = item
	}
	return result
}

func appendAttrsToGroup(groups []string, actualAttrs, newAttrs []slog.Attr) []slog.Attr {
	if len(groups) == 0 {
		return uniqAttrs(append(actualAttrs, newAttrs...))
	}

	for i := range actualAttrs {
		attr := actualAttrs[i]
		if attr.Key == groups[0] && attr.Value.Kind() == slog.KindGroup {
			actualAttrs[i] = slog.Group(groups[0], attrSliceToAny(appendAttrsToGroup(groups[1:], attr.Value.Group(), newAttrs))...)
			return actualAttrs
		}
	}

	return uniqAttrs(
		append(
			actualAttrs,
			slog.Group(
				groups[0],
				attrSliceToAny(appendAttrsToGroup(groups[1:], []slog.Attr{}, newAttrs))...,
			),
		),
	)
}

func uniqAttrs(attrs []slog.Attr) []slog.Attr {
	result := make([]slog.Attr, 0, len(attrs))
	seen := make(map[string]int, len(attrs))
	seenIndex := 0

	for _, item := range attrs {
		key := item.Key

		if index, ok := seen[key]; ok {
			result[index] = item
			continue
		}

		seen[key] = seenIndex
		seenIndex++
		result = append(result, item)
	}

	return result
}
