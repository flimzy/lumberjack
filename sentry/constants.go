package sentry

// Common log fields.
const (
	FieldError       = "error"
	FieldRequest     = "request"
	FieldEnvironment = "env"
	FieldBuildInfo   = "build"
)
