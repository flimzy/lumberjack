package sentry

import (
	"context"
	"errors"
	"log/slog"
	"net/http"
	"slices"
	"time"

	"github.com/getsentry/sentry-go"

	"gitlab.com/flimzy/lumberjack/handlerhelper"
)

type sentryHub interface {
	Flush(time.Duration) bool
	CaptureEvent(*sentry.Event) *sentry.EventID
}

// Handler is a [log/slog.Handler] for Sentry.
type Handler struct {
	event   *sentry.Event
	hub     sentryHub
	level   slog.Leveler
	acc     handlerhelper.MapAccumulator
	replace func([]string, slog.Attr) slog.Attr
}

var _ slog.Handler = (*Handler)(nil)

// New returns a new Sentry logging handler.
func New(level slog.Leveler, options sentry.ClientOptions) (*Handler, error) {
	client, err := sentry.NewClient(options)
	if err != nil {
		return nil, err
	}

	return NewFromClient(level, client), nil
}

// NewFromClient initializes a new Handler which sends logs to the provided
// sentry client.
func NewFromClient(level slog.Leveler, client *sentry.Client) *Handler {
	return &Handler{
		level: level,
		hub:   sentry.NewHub(client, sentry.NewScope()),
	}
}

// ReplaceAttr can be used to change the default keys of the built-in
// attributes, convert types (for example, to replace a `time.Time` with the
// integer seconds since the Unix epoch), sanitize personal information, or
// remove attributes from the output.
func (h *Handler) ReplaceAttr(fn func(groups []string, a slog.Attr) slog.Attr) {
	h.replace = fn
}

// Flush waits until the underlying Sentry transport sends any buffered events,
// blocking for at most the given timeout. It returns false if the timeout was
// reached, in which case some events may not have been sent.
func (h *Handler) Flush(timeout time.Duration) bool {
	return h.hub.Flush(timeout)
}

// Enabled returns true if level is at least to the configured minimum level.
func (h *Handler) Enabled(_ context.Context, level slog.Level) bool {
	minLevel := slog.LevelInfo
	if h.level != nil {
		minLevel = h.level.Level()
	}
	return level >= minLevel
}

// cloneHub is a function, rather than a method, to accomodote tests. It
// properly clones actual *sentry.Hub instances, and returns all others
// unaltered.
func cloneHub(h sentryHub) sentryHub {
	if hub, ok := h.(*sentry.Hub); ok {
		return hub.Clone()
	}
	return h
}

func (h *Handler) clone() *Handler {
	var eventCopy *sentry.Event
	if h.event != nil {
		eventCopy = new(sentry.Event)
		*eventCopy = *h.event
	}
	return &Handler{
		event:   eventCopy,
		hub:     cloneHub(h.hub),
		level:   h.level,
		acc:     h.acc,
		replace: h.replace,
	}
}

func (h *Handler) setRequest(req *http.Request) {
	if h.event == nil {
		h.event = sentry.NewEvent()
	}
	h.event.Request = sentry.NewRequest(req)
}

func (h *Handler) setException(err error) {
	excs := []sentry.Exception{}
	var last *sentry.Exception
	for ; err != nil; err = errors.Unwrap(err) {
		exc := sentry.Exception{
			Type:       "error",
			Value:      err.Error(),
			Stacktrace: sentry.ExtractStacktrace(err),
		}
		if last != nil && exc.Value == last.Value {
			if last.Stacktrace == nil {
				last.Stacktrace = exc.Stacktrace
				continue
			}
			if exc.Stacktrace == nil {
				continue
			}
		}
		excs = append(excs, exc)
		last = &excs[len(excs)-1]
	}
	if len(excs) == 0 {
		return
	}

	slices.Reverse(excs)

	if h.event == nil {
		h.event = sentry.NewEvent()
	}
	h.event.Exception = excs
}

func (h *Handler) filterAttr(groups []string, attr slog.Attr) slog.Attr {
	if h.replace != nil {
		attr = h.replace(groups, attr)
		if attr.Key == "" && attr.Value.Equal(slog.Value{}) {
			return slog.Attr{}
		}
	}
	if attr.Value.Kind() == slog.KindDuration {
		return slog.String(attr.Key, attr.Value.Duration().String())
	}
	switch len(groups) {
	case 0:
		// continue
	case 1:
		if groups[0] == FieldBuildInfo && attr.Key == "sha" {
			// This is passed as part of the Sentry config, so needn't be
			// duplicated here.
			return slog.Attr{}
		}
	default:
		return attr
	}
	switch attr.Key {
	// Environment field is duplicated in the sentry client config.
	case FieldEnvironment:
		return slog.Attr{}
	case FieldRequest:
		if req, ok := attr.Value.Any().(*http.Request); ok {
			h.setRequest(req)
			return slog.Attr{}
		}
	case FieldError:
		if err, ok := attr.Value.Any().(error); ok {
			h.setException(err)
			return slog.String(attr.Key, err.Error()) // To avoid any JSON marshaling shenanigans
		}
	}
	return attr
}

// Handle logs r Sentry.
func (h *Handler) Handle(_ context.Context, r slog.Record) error {
	extra := h.acc.ResultF(r, h.filterAttr)
	event := h.event
	if event == nil {
		event = sentry.NewEvent()
	}
	event.Timestamp = r.Time
	event.Extra = extra
	event.Level = levelMap[r.Level]
	event.Message = r.Message

	if id := h.hub.CaptureEvent(event); id == nil {
		return errors.New("failed to send to sentry")
	}
	return nil
}

//nolint:gochecknoglobals // map can't be const
var levelMap = map[slog.Level]sentry.Level{
	slog.LevelDebug: sentry.LevelDebug,
	slog.LevelInfo:  sentry.LevelInfo,
	slog.LevelWarn:  sentry.LevelWarning,
	slog.LevelError: sentry.LevelError,
}

// WithAttrs returns a clone of h, with attrs appended.
func (h *Handler) WithAttrs(attrs []slog.Attr) slog.Handler {
	h2 := h.clone()
	h2.acc = h.acc.WithAttrsF(attrs, h2.filterAttr)
	return h2
}

// WithGroup returns a clone of h, with the named group added.
func (h *Handler) WithGroup(name string) slog.Handler {
	h2 := h.clone()
	h2.acc = h.acc.WithGroup(name)
	return h2
}
