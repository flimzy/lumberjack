package sentry

import (
	"log/slog"
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	pkgerr "github.com/pkg/errors"
)

type sentrySpy struct {
	logged sentry.Event
}

var _ sentryHub = (*sentrySpy)(nil)

func (*sentrySpy) Flush(time.Duration) bool { return true }

func (s *sentrySpy) CaptureEvent(e *sentry.Event) *sentry.EventID {
	s.logged = *e
	id := sentry.EventID("xxx")
	return &id
}

func TestHandler(t *testing.T) {
	tests := []struct {
		name    string
		fn      func(*slog.Logger)
		replace func([]string, slog.Attr) slog.Attr
		want    sentry.Event
	}{
		{
			name: "simple log",
			fn: func(l *slog.Logger) {
				l.Info("m")
			},
			want: sentry.Event{
				Contexts: map[string]map[string]any{},
				Extra:    map[string]any{},
				Level:    sentry.LevelInfo,
				Message:  "m",
				Tags:     map[string]string{},
				Modules:  map[string]string{},
			},
		},
		{
			name: "attrs",
			fn: func(l *slog.Logger) {
				l.With("foo", "bar").Info("m2")
			},
			want: sentry.Event{
				Contexts: map[string]map[string]any{},
				Extra:    map[string]any{"foo": string("bar")},
				Level:    sentry.LevelInfo,
				Message:  "m2",
				Tags:     map[string]string{},
				Modules:  map[string]string{},
			},
		},
		{
			name: "http request",
			fn: func(l *slog.Logger) {
				l.With(FieldRequest, &http.Request{
					URL:    &url.URL{},
					Method: http.MethodGet,
				},
				).Info("m3")
			},
			want: sentry.Event{
				Contexts: map[string]map[string]any{},
				Extra:    map[string]any{},
				Level:    sentry.LevelInfo,
				Message:  "m3",
				Tags:     map[string]string{},
				Modules:  map[string]string{},
				Request: &sentry.Request{
					URL:     "http://",
					Method:  http.MethodGet,
					Headers: map[string]string{"Host": ""},
				},
			},
		},
		{
			name: "error with stack trace",
			fn: func(l *slog.Logger) {
				l.With(FieldError, pkgerr.New("oh noes")).Info("m4")
			},
			want: sentry.Event{
				Contexts: map[string]map[string]any{},
				Extra:    map[string]any{"error": string("oh noes")},
				Level:    sentry.LevelInfo,
				Message:  "m4",
				Tags:     map[string]string{},
				Modules:  map[string]string{},
				Exception: []sentry.Exception{
					{
						Type:  "error",
						Value: "oh noes",
						Stacktrace: &sentry.Stacktrace{
							Frames: []sentry.Frame{
								{Function: "TestHandler.func9"},
								{Function: "TestHandler.func4"},
							},
						},
					},
				},
			},
		},
		{
			name: "replace attr",
			fn: func(l *slog.Logger) {
				l.With("foo", "bar").Info("m5")
			},
			replace: func(_ []string, attr slog.Attr) slog.Attr {
				if attr.Key == "foo" {
					return slog.String("foo", "baz")
				}
				return attr
			},
			want: sentry.Event{
				Contexts: map[string]map[string]any{},
				Extra:    map[string]any{"foo": string("baz")},
				Level:    sentry.LevelInfo,
				Message:  "m5",
				Tags:     map[string]string{},
				Modules:  map[string]string{},
			},
		},
		{
			name: "remove attr",
			fn: func(l *slog.Logger) {
				l.With("foo", "bar").Info("m5")
			},
			replace: func(_ []string, attr slog.Attr) slog.Attr {
				if attr.Key == "foo" {
					return slog.Attr{}
				}
				return attr
			},
			want: sentry.Event{
				Contexts: map[string]map[string]any{},
				Extra:    map[string]any{},
				Level:    sentry.LevelInfo,
				Message:  "m5",
				Tags:     map[string]string{},
				Modules:  map[string]string{},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &sentrySpy{}
			h := &Handler{
				level:   slog.LevelInfo,
				hub:     l,
				replace: tt.replace,
			}
			sl := slog.New(h)
			tt.fn(sl)
			if d := cmp.Diff(tt.want, l.logged,
				cmpopts.IgnoreUnexported(sentry.Event{}),
				cmpopts.IgnoreFields(sentry.Event{}, "Timestamp"),
				cmpopts.IgnoreFields(sentry.Frame{}, "Lineno", "Module", "InApp", "AbsPath"),
			); d != "" {
				t.Error(d)
			}
		})
	}
}
